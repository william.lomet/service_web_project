    CREATE TABLE Client(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    surname VARCHAR(50),
    identifiant VARCHAR(50),
    email VARCHAR(50),
    mdp VARCHAR(250),
    PRIMARY KEY(id)
    );

    CREATE TABLE City(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    country VARCHAR(50),
    PRIMARY KEY(id)
    );

    CREATE TABLE Train(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    nb_place_all SMALLINT,
    nb_business INT,
    nb_standard INT,
    nb_first INT,
    PRIMARY KEY(id)
    );

    CREATE TABLE Type_Class(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    PRIMARY KEY(id)
    );

    CREATE TABLE Trajet(
    id INT NOT NULL AUTO_INCREMENT,
    start_city INT,
    arrival_city INT,
    start_date DATE,
    arrival_date DATE,
    start_hour TIME,
    arrival_hour TIME,
    id_train INT,
    id_start_city INT,
    id_arrival_city INT,
    PRIMARY KEY(id),
    FOREIGN KEY(id_train) REFERENCES Train(id),
    FOREIGN KEY(id_start_city) REFERENCES City(id),
    FOREIGN KEY(id_arrival_city) REFERENCES City(id)
    );

    CREATE TABLE Ticket(
    id INT NOT NULL AUTO_INCREMENT,
    price VARCHAR(50),
    id_type_class INT,
    id_trajet INT NOT NULL,
    id_client INT,
    PRIMARY KEY(id),
    FOREIGN KEY(id_type_class) REFERENCES Type_Class(id),
    FOREIGN KEY(id_trajet) REFERENCES Trajet(id),
    FOREIGN KEY(id_client) REFERENCES Client(id)
    );
