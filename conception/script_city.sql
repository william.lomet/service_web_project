-- France
INSERT INTO `city` (`name`, `country`)
VALUES
    ('Paris', 'France'),
    ('Marseille', 'France'),
    ('Lyon', 'France'),
    ('Toulouse', 'France'),
    ('Nice', 'France'),
    ('Bordeaux', 'France'),
    ('Lille', 'France'),
    ('Strasbourg', 'France'),
    ('Nantes', 'France'),
    ('Montpellier', 'France');

-- Allemagne
INSERT INTO `city` (`name`, `country`)
VALUES
    ('Berlin', 'Allemagne'),
    ('Hambourg', 'Allemagne'),
    ('Munich', 'Allemagne'),
    ('Cologne', 'Allemagne'),
    ('Francfort', 'Allemagne'),
    ('Stuttgart', 'Allemagne'),
    ('Düsseldorf', 'Allemagne'),
    ('Dortmund', 'Allemagne'),
    ('Essen', 'Allemagne'),
    ('Leipzig', 'Allemagne');

-- Répétez ces INSERT statements pour d'autres pays européens...

-- Espagne
INSERT INTO `city` (`name`, `country`)
VALUES
    ('Madrid', 'Espagne'),
    ('Barcelone', 'Espagne'),
    ('Valence', 'Espagne'),
    ('Séville', 'Espagne'),
    ('Saragosse', 'Espagne'),
    ('Malaga', 'Espagne'),
    ('Murcie', 'Espagne'),
    ('Palma', 'Espagne'),
    ('Las Palmas', 'Espagne'),
    ('Bilbao', 'Espagne');

    -- Pologne
INSERT INTO `city` (`name`, `country`)
VALUES
    ('Varsovie', 'Pologne'),
    ('Cracovie', 'Pologne'),
    ('Łódź', 'Pologne'),
    ('Wrocław', 'Pologne'),
    ('Poznań', 'Pologne'),
    ('Gdańsk', 'Pologne'),
    ('Szczecin', 'Pologne'),
    ('Bydgoszcz', 'Pologne'),
    ('Lublin', 'Pologne'),
    ('Katowice', 'Pologne');

-- Roumanie
INSERT INTO `city` (`name`, `country`)
VALUES
    ('Bucarest', 'Roumanie'),
    ('Cluj-Napoca', 'Roumanie'),
    ('Iași', 'Roumanie'),
    ('Timișoara', 'Roumanie'),
    ('Craiova', 'Roumanie'),
    ('Constanța', 'Roumanie'),
    ('Oradea', 'Roumanie'),
    ('Brașov', 'Roumanie'),
    ('Galați', 'Roumanie'),
    ('Ploiești', 'Roumanie');

-- Suisse
INSERT INTO `city` (`name`, `country`)
VALUES
    ('Zurich', 'Suisse'),
    ('Genève', 'Suisse'),
    ('Bâle', 'Suisse'),
    ('Berne', 'Suisse'),
    ('Lausanne', 'Suisse'),
    ('Lucerne', 'Suisse'),
    ('Saint-Gall', 'Suisse'),
    ('Lugano', 'Suisse'),
    ('Winterthour', 'Suisse'),
    ('Thoune', 'Suisse');

-- Portugal
INSERT INTO `city` (`name`, `country`)
VALUES
    ('Lisbonne', 'Portugal'),
    ('Porto', 'Portugal'),
    ('Vila Nova de Gaia', 'Portugal'),
    ('Amadora', 'Portugal'),
    ('Braga', 'Portugal'),
    ('Funchal', 'Portugal'),
    ('Coimbra', 'Portugal'),
    ('Setúbal', 'Portugal'),
    ('Queluz', 'Portugal'),
    ('Vila Franca de Xira', 'Portugal');

-- Russie
INSERT INTO `city` (`name`, `country`)
VALUES
    ('Moscou', 'Russie'),
    ('Saint-Pétersbourg', 'Russie'),
    ('Novossibirsk', 'Russie'),
    ('Ekaterinbourg', 'Russie'),
    ('Nijni Novgorod', 'Russie'),
    ('Kazan', 'Russie'),
    ('Tcheliabinsk', 'Russie'),
    ('Omsk', 'Russie'),
    ('Samara', 'Russie'),
    ('Rostov-sur-le-Don', 'Russie');