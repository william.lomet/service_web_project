-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           8.0.30 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Listage de la structure de la base pour web_service_project_mk_lm
CREATE DATABASE IF NOT EXISTS `web_service_project_mk_lm` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `web_service_project_mk_lm`;

-- Listage de la structure de table web_service_project_mk_lm. city
CREATE TABLE IF NOT EXISTS `city` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table web_service_project_mk_lm.city : ~80 rows (environ)
REPLACE INTO `city` (`id`, `name`, `country`) VALUES
	(1, 'Paris', 'France'),
	(2, 'Marseille', 'France'),
	(3, 'Lyon', 'France'),
	(4, 'Toulouse', 'France'),
	(5, 'Nice', 'France'),
	(6, 'Bordeaux', 'France'),
	(7, 'Lille', 'France'),
	(8, 'Strasbourg', 'France'),
	(9, 'Nantes', 'France'),
	(10, 'Montpellier', 'France'),
	(11, 'Berlin', 'Allemagne'),
	(12, 'Hambourg', 'Allemagne'),
	(13, 'Munich', 'Allemagne'),
	(14, 'Cologne', 'Allemagne'),
	(15, 'Francfort', 'Allemagne'),
	(16, 'Stuttgart', 'Allemagne'),
	(17, 'Düsseldorf', 'Allemagne'),
	(18, 'Dortmund', 'Allemagne'),
	(19, 'Essen', 'Allemagne'),
	(20, 'Leipzig', 'Allemagne'),
	(21, 'Madrid', 'Espagne'),
	(22, 'Barcelone', 'Espagne'),
	(23, 'Valence', 'Espagne'),
	(24, 'Séville', 'Espagne'),
	(25, 'Saragosse', 'Espagne'),
	(26, 'Malaga', 'Espagne'),
	(27, 'Murcie', 'Espagne'),
	(28, 'Palma', 'Espagne'),
	(29, 'Las Palmas', 'Espagne'),
	(30, 'Bilbao', 'Espagne'),
	(31, 'Varsovie', 'Pologne'),
	(32, 'Cracovie', 'Pologne'),
	(33, 'Łódź', 'Pologne'),
	(34, 'Wrocław', 'Pologne'),
	(35, 'Poznań', 'Pologne'),
	(36, 'Gdańsk', 'Pologne'),
	(37, 'Szczecin', 'Pologne'),
	(38, 'Bydgoszcz', 'Pologne'),
	(39, 'Lublin', 'Pologne'),
	(40, 'Katowice', 'Pologne'),
	(41, 'Bucarest', 'Roumanie'),
	(42, 'Cluj-Napoca', 'Roumanie'),
	(43, 'Iași', 'Roumanie'),
	(44, 'Timișoara', 'Roumanie'),
	(45, 'Craiova', 'Roumanie'),
	(46, 'Constanța', 'Roumanie'),
	(47, 'Oradea', 'Roumanie'),
	(48, 'Brașov', 'Roumanie'),
	(49, 'Galați', 'Roumanie'),
	(50, 'Ploiești', 'Roumanie'),
	(51, 'Zurich', 'Suisse'),
	(52, 'Genève', 'Suisse'),
	(53, 'Bâle', 'Suisse'),
	(54, 'Berne', 'Suisse'),
	(55, 'Lausanne', 'Suisse'),
	(56, 'Lucerne', 'Suisse'),
	(57, 'Saint-Gall', 'Suisse'),
	(58, 'Lugano', 'Suisse'),
	(59, 'Winterthour', 'Suisse'),
	(60, 'Thoune', 'Suisse'),
	(61, 'Lisbonne', 'Portugal'),
	(62, 'Porto', 'Portugal'),
	(63, 'Vila Nova de Gaia', 'Portugal'),
	(64, 'Amadora', 'Portugal'),
	(65, 'Braga', 'Portugal'),
	(66, 'Funchal', 'Portugal'),
	(67, 'Coimbra', 'Portugal'),
	(68, 'Setúbal', 'Portugal'),
	(69, 'Queluz', 'Portugal'),
	(70, 'Vila Franca de Xira', 'Portugal'),
	(71, 'Moscou', 'Russie'),
	(72, 'Saint-Pétersbourg', 'Russie'),
	(73, 'Novossibirsk', 'Russie'),
	(74, 'Ekaterinbourg', 'Russie'),
	(75, 'Nijni Novgorod', 'Russie'),
	(76, 'Kazan', 'Russie'),
	(77, 'Tcheliabinsk', 'Russie'),
	(78, 'Omsk', 'Russie'),
	(79, 'Samara', 'Russie'),
	(80, 'Rostov-sur-le-Don', 'Russie');

-- Listage de la structure de table web_service_project_mk_lm. client
CREATE TABLE IF NOT EXISTS `client` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `identifiant` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mdp` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table web_service_project_mk_lm.client : ~10 rows (environ)
REPLACE INTO `client` (`id`, `name`, `surname`, `identifiant`, `email`, `mdp`) VALUES
	(1, 'John', 'Doe', 'johndoe', 'john@example.com', 'mdp'),
	(2, 'Jane', 'Smith', 'janesmith', 'jane@example.com', 'mdp'),
	(3, 'Robert', 'Johnson', 'robertjohnson', 'robert@example.com', 'mdp'),
	(4, 'Emily', 'Davis', 'emilydavis', 'emily@example.com', 'mdp'),
	(5, 'Michael', 'Brown', 'michaelbrown', 'michael@example.com', 'mdp'),
	(6, 'Sarah', 'Lee', 'sarahlee', 'sarah@example.com', 'mdp'),
	(7, 'William', 'Clark', 'williamclark', 'william@example.com', 'mdp'),
	(8, 'Jennifer', 'Wilson', 'jenniferwilson', 'jennifer@example.com', 'mdp'),
	(9, 'David', 'Martinez', 'davidmartinez', 'david@example.com', 'mdp'),
	(10, 'Mary', 'Taylor', 'marytaylor', 'mary@example.com', 'mdp');

-- Listage de la structure de table web_service_project_mk_lm. ticket
CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `price` varchar(50) DEFAULT NULL,
  `id_type_class` int DEFAULT NULL,
  `id_trajet` int NOT NULL,
  `id_client` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_type_class` (`id_type_class`),
  KEY `id_trajet` (`id_trajet`),
  KEY `id_client` (`id_client`),
  CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`id_type_class`) REFERENCES `type_class` (`id`),
  CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`id_trajet`) REFERENCES `trajet` (`id`),
  CONSTRAINT `ticket_ibfk_3` FOREIGN KEY (`id_client`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table web_service_project_mk_lm.ticket : ~0 rows (environ)

-- Listage de la structure de table web_service_project_mk_lm. train
CREATE TABLE IF NOT EXISTS `train` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `nb_place_all` smallint DEFAULT NULL,
  `nb_business` int DEFAULT NULL,
  `nb_standard` int DEFAULT NULL,
  `nb_first` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table web_service_project_mk_lm.train : ~10 rows (environ)
REPLACE INTO `train` (`id`, `name`, `nb_place_all`, `nb_business`, `nb_standard`, `nb_first`) VALUES
	(1, 'Train_1', 138, 35, 74, 77),
	(2, 'Train_2', 135, 27, 60, 58),
	(3, 'Train_3', 160, 32, 53, 57),
	(4, 'Train_4', 194, 35, 78, 61),
	(5, 'Train_5', 144, 28, 61, 58),
	(6, 'Train_6', 147, 35, 69, 50),
	(7, 'Train_7', 160, 27, 65, 50),
	(8, 'Train_8', 186, 37, 67, 67),
	(9, 'Train_9', 136, 27, 61, 63),
	(10, 'Train_10', 131, 28, 75, 65);

-- Listage de la structure de table web_service_project_mk_lm. trajet
CREATE TABLE IF NOT EXISTS `trajet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL,
  `arrival_date` date NOT NULL,
  `start_hour` time NOT NULL,
  `arrival_hour` time NOT NULL,
  `id_train` int DEFAULT NULL,
  `id_start_city` int DEFAULT NULL,
  `id_arrival_city` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_train` (`id_train`),
  KEY `id_start_city` (`id_start_city`),
  KEY `id_arrival_city` (`id_arrival_city`),
  CONSTRAINT `trajet_ibfk_1` FOREIGN KEY (`id_train`) REFERENCES `train` (`id`),
  CONSTRAINT `trajet_ibfk_2` FOREIGN KEY (`id_start_city`) REFERENCES `city` (`id`),
  CONSTRAINT `trajet_ibfk_3` FOREIGN KEY (`id_arrival_city`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table web_service_project_mk_lm.trajet : ~50 rows (environ)
REPLACE INTO `trajet` (`id`, `start_date`, `arrival_date`, `start_hour`, `arrival_hour`, `id_train`, `id_start_city`, `id_arrival_city`) VALUES
	(29, '2023-09-01', '2023-09-10', '06:42:56', '14:06:47', 1, 1, 50),
	(30, '2023-09-22', '2023-09-23', '06:50:21', '14:05:45', 1, 2, 42),
	(31, '2023-09-10', '2023-09-13', '06:56:22', '14:09:39', 1, 3, 62),
	(32, '2023-09-02', '2023-09-07', '06:15:06', '14:32:23', 1, 4, 25),
	(33, '2023-09-12', '2023-09-30', '06:11:25', '14:36:20', 1, 5, 69),
	(36, '2023-09-08', '2023-09-21', '06:06:38', '14:27:38', 2, 1, 71),
	(37, '2023-09-14', '2023-09-19', '06:43:27', '14:13:45', 2, 2, 9),
	(38, '2023-09-08', '2023-09-30', '06:48:35', '14:43:09', 2, 3, 48),
	(39, '2023-09-16', '2023-09-25', '06:56:15', '14:44:09', 2, 4, 48),
	(40, '2023-09-15', '2023-09-21', '06:23:19', '14:07:20', 2, 5, 31),
	(43, '2023-10-09', '2023-10-16', '06:17:01', '14:07:55', 3, 1, 21),
	(44, '2023-09-15', '2023-09-22', '06:14:58', '14:48:36', 3, 2, 52),
	(45, '2023-09-11', '2023-09-26', '06:37:25', '14:57:02', 3, 3, 25),
	(46, '2023-09-13', '2023-09-15', '06:47:40', '14:44:50', 3, 4, 1),
	(47, '2023-09-16', '2023-09-17', '06:33:02', '14:41:46', 3, 5, 63),
	(50, '2023-09-12', '2023-09-15', '06:20:23', '14:40:29', 4, 1, 32),
	(51, '2023-09-16', '2023-09-21', '06:37:02', '14:36:00', 4, 2, 19),
	(52, '2023-09-03', '2023-09-25', '06:17:13', '14:30:51', 4, 3, 60),
	(53, '2023-09-05', '2023-09-17', '06:33:07', '14:07:56', 4, 4, 29),
	(54, '2023-09-16', '2023-09-20', '06:15:56', '14:45:03', 4, 5, 2),
	(57, '2023-09-13', '2023-09-19', '06:20:00', '14:03:12', 5, 1, 71),
	(58, '2023-09-24', '2023-10-09', '06:13:05', '14:36:44', 5, 2, 69),
	(59, '2023-09-02', '2023-09-20', '06:47:31', '14:39:32', 5, 3, 61),
	(60, '2023-09-04', '2023-09-11', '06:03:58', '14:50:57', 5, 4, 21),
	(61, '2023-09-13', '2023-09-18', '06:01:35', '14:48:17', 5, 5, 4),
	(64, '2023-09-10', '2023-09-14', '06:53:45', '14:08:26', 6, 1, 66),
	(65, '2023-08-24', '2023-09-12', '06:58:58', '14:57:49', 6, 2, 23),
	(66, '2023-09-17', '2023-09-21', '06:25:03', '14:59:51', 6, 3, 27),
	(67, '2023-09-03', '2023-09-12', '06:00:11', '14:55:58', 6, 4, 4),
	(68, '2023-09-10', '2023-09-15', '06:32:33', '14:57:24', 6, 5, 14),
	(71, '2023-09-20', '2023-09-22', '06:14:10', '14:10:26', 7, 1, 54),
	(72, '2023-09-01', '2023-09-04', '06:13:33', '14:59:33', 7, 2, 64),
	(73, '2023-09-09', '2023-09-14', '06:52:28', '14:18:19', 7, 3, 54),
	(74, '2023-09-12', '2023-09-27', '06:38:26', '14:04:32', 7, 4, 38),
	(75, '2023-09-15', '2023-09-18', '06:59:51', '14:07:34', 7, 5, 44),
	(78, '2023-09-29', '2023-10-02', '06:55:19', '14:16:47', 8, 1, 12),
	(79, '2023-09-12', '2023-09-13', '06:18:51', '14:12:59', 8, 2, 24),
	(80, '2023-09-16', '2023-09-21', '06:26:29', '14:18:05', 8, 3, 39),
	(81, '2023-09-05', '2023-09-10', '06:27:03', '14:29:37', 8, 4, 54),
	(82, '2023-09-15', '2023-09-18', '06:59:42', '14:47:11', 8, 5, 45),
	(85, '2023-09-08', '2023-09-16', '06:31:10', '14:04:23', 9, 1, 63),
	(86, '2023-09-04', '2023-09-07', '06:51:37', '14:55:56', 9, 2, 73),
	(87, '2023-09-22', '2023-09-27', '06:23:23', '14:30:20', 9, 3, 17),
	(88, '2023-09-05', '2023-09-10', '06:12:09', '14:13:41', 9, 4, 63),
	(89, '2023-09-26', '2023-09-27', '06:45:06', '14:10:57', 9, 5, 22),
	(92, '2023-09-15', '2023-09-19', '06:13:18', '14:18:59', 10, 1, 4),
	(93, '2023-09-04', '2023-09-06', '06:13:39', '14:07:46', 10, 2, 37),
	(94, '2023-09-29', '2023-10-02', '06:28:17', '14:18:27', 10, 3, 26),
	(95, '2023-09-08', '2023-09-08', '06:04:37', '14:47:10', 10, 4, 37),
	(96, '2023-09-24', '2023-10-01', '06:30:55', '14:39:00', 10, 5, 7);

-- Listage de la structure de table web_service_project_mk_lm. type_class
CREATE TABLE IF NOT EXISTS `type_class` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table web_service_project_mk_lm.type_class : ~3 rows (environ)
REPLACE INTO `type_class` (`id`, `name`) VALUES
	(1, 'First'),
	(2, 'Premium'),
	(3, 'Standard');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
