from django.shortcuts import render
from pymysql import IntegrityError

# Create your views here.
from spyne.application import Application
from spyne.decorator import rpc
from spyne.service import ServiceBase
from spyne.model.primitive import String
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from django.http import JsonResponse
from django.utils.dateparse import parse_date, parse_time
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta

from django.db import connection, transaction


class MonServiceSOAP(ServiceBase):
    @rpc(String, _returns=String)
    def get_info(ctx, param):
       
        return "Resultat"

# Configuration de l'application SOAP
application = Application([MonServiceSOAP],
                          tns='mon.namespace',
                          in_protocol=Soap11(validator='lxml'),
                          out_protocol=Soap11())

# Vue Django pour le service SOAP
mon_service_soap = DjangoApplication(application)

def tous_les_trajets(request):
    with connection.cursor() as cursor:
        cursor.execute("""SELECT trajet.id, trajet.start_date, trajet.arrival_date, 
                                 trajet.start_hour, trajet.arrival_hour, trajet.id_train,
                                 depart.name as start_city_name, arrivee.name as arrival_city_name
                          FROM trajet
                          INNER JOIN city as depart ON trajet.id_start_city = depart.id
                          INNER JOIN city as arrivee ON trajet.id_arrival_city = arrivee.id""")
        trajets = cursor.fetchall()

    # Convertir les résultats en format JSON
    trajets_liste = [dict(zip([column[0] for column in cursor.description], row)) for row in trajets]
    return JsonResponse(trajets_liste, safe=False)



def ajouter_trajet(request, depart, arrivee, date_depart, heure_depart, date_arrivee, heure_arrivee, numero_train):
    # Parser les dates et heures
    date_de_depart = parse_date(date_depart)
    heure_de_depart = parse_time(heure_depart)
    date_d_arrivee = parse_date(date_arrivee)
    heure_d_arrivee = parse_time(heure_arrivee)

    # Préparer la requête SQL
    insert_query = """
    INSERT INTO trajet (start_date, start_hour, arrival_date, arrival_hour, id_train, id_start_city, id_arrival_city)
    SELECT %s, %s, %s, %s, train.id, city_depart.id, city_arrivee.id
    FROM train, city as city_depart, city as city_arrivee
    WHERE train.name = %s AND city_depart.name = %s AND city_arrivee.name = %s
    """

    with connection.cursor() as cursor:
        cursor.execute(insert_query, [
            date_de_depart, heure_de_depart, date_d_arrivee, heure_d_arrivee,
            numero_train, depart, arrivee
        ])
        connection.commit()

    return HttpResponse("Trajet ajouté avec succès !", status=201)


def reserver_place(request, id_trajet, classe, id_client, nombre_places):
    try:
        with transaction.atomic():
            with connection.cursor() as cursor:
                # Vérifier la disponibilité
                cursor.execute(f"""
                    SELECT train.nb_{classe.lower()} - trajet.nb_{classe.lower()}_reserved as available
                    FROM trajet
                    JOIN train ON trajet.id_train = train.id
                    WHERE trajet.id = %s
                """, [id_trajet])
                available = cursor.fetchone()[0]
                
                if available >= nombre_places:
                    # Mettre à jour les places réservées dans le trajet
                    cursor.execute(f"UPDATE trajet SET nb_{classe.lower()}_reserved = nb_{classe.lower()}_reserved + %s WHERE id = %s", [nombre_places, id_trajet])

                    # Insérer la réservation
                    cursor.execute("INSERT INTO reservation (id_client, id_trajet, classe, nombre_places) VALUES (%s, %s, %s, %s)", [id_client, id_trajet, classe.capitalize(), nombre_places])
                else:
                    return HttpResponse("Pas assez de places disponibles.", status=400)

        return HttpResponse("Réservation effectuée avec succès.", status=200)
    except Exception as e:
        return HttpResponse(str(e), status=500)


def trajvilles(request):
    # Récupérer les paramètres de la requête
    depart = request.GET.get('depart')
    arrivee = request.GET.get('arrivee')
    date_depart = request.GET.get('date_depart')
    date_arrivee = request.GET.get('date_arrivee')
    heure_depart = request.GET.get('heure_depart')  
    heure_arrivee = request.GET.get('heure_arrivee')

    date_format = "%Y-%m-%d"
    
    # Début de la requête SQL
    raw_query = """
        SELECT city_depart.name as depart_city_name, city_arrivee.name as arrivee_city_name,
           trajet.id, trajet.start_date, trajet.arrival_date, trajet.start_hour, trajet.arrival_hour,
           train.name as train_name,
           train.nb_place_all - (trajet.nb_premium_reserved + trajet.nb_standard_reserved + trajet.nb_first_reserved) as nb_place_disponible_all,
           train.nb_premium - trajet.nb_premium_reserved as nb_place_disponible_premium,
           train.nb_standard - trajet.nb_standard_reserved as nb_place_disponible_standard,
           train.nb_first - trajet.nb_first_reserved as nb_place_disponible_first
    FROM trajet
    INNER JOIN city as city_depart ON trajet.id_start_city = city_depart.id
    INNER JOIN city as city_arrivee ON trajet.id_arrival_city = city_arrivee.id
    INNER JOIN train ON trajet.id_train = train.id
    """

    # Préparer les paramètres pour la clause WHERE
    params = []
    where_clauses = []

    if depart:
        where_clauses.append("city_depart.name = %s")
        params.append(depart)
    if arrivee:
        where_clauses.append("city_arrivee.name = %s")
        params.append(arrivee)

    # Ajouter les conditions de date et d'heure
    if date_depart and date_depart != 'none':
        date_obj = datetime.strptime(date_depart, date_format)
        where_clauses.append("trajet.start_date >= %s")
        params.append(date_obj.date())  
    if date_arrivee and date_arrivee != 'none':
        date_obj = datetime.strptime(date_arrivee, date_format)
        where_clauses.append("trajet.arrival_date <= %s")
        params.append(date_obj.date())
    if heure_depart not in (None, 'none'):
        where_clauses.append("trajet.start_hour >= %s")
        params.append(heure_depart)
    if heure_arrivee not in (None, 'none'):
        where_clauses.append("trajet.arrival_hour <= %s")
        params.append(heure_arrivee)

    # Ajouter les conditions à la requête SQL
    if where_clauses:
        raw_query += " WHERE " + " AND ".join(where_clauses)
    
    # Exécuter la requête SQL
    with connection.cursor() as cursor:
        cursor.execute(raw_query, params)
        columns = [col[0] for col in cursor.description]
        trajets = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
    
    # Retourner les données en JSON
    return JsonResponse(trajets, safe=False)


def infos_trajet(request, trajet_id):
    # Début de la requête SQL pour récupérer les informations du trajet
    raw_query = """
        SELECT city_depart.name as depart_city_name, city_arrivee.name as arrivee_city_name,
               trajet.id, trajet.start_date, trajet.arrival_date, trajet.start_hour, trajet.arrival_hour,
               train.name as train_name, 
               train.nb_place_all - (trajet.nb_premium_reserved + trajet.nb_standard_reserved + trajet.nb_first_reserved) as nb_place_disponible_all,
               train.nb_premium - trajet.nb_premium_reserved as nb_place_disponible_premium,
               train.nb_standard - trajet.nb_standard_reserved as nb_place_disponible_standard,
               train.nb_first - trajet.nb_first_reserved as nb_place_disponible_first
        FROM trajet
        INNER JOIN city as city_depart ON trajet.id_start_city = city_depart.id
        INNER JOIN city as city_arrivee ON trajet.id_arrival_city = city_arrivee.id
        INNER JOIN train ON trajet.id_train = train.id
        WHERE trajet.id = %s
    """

    # Exécuter la requête SQL avec l'ID du trajet fourni
    with connection.cursor() as cursor:
        cursor.execute(raw_query, [trajet_id])
        columns = [col[0] for col in cursor.description]
        trajet = cursor.fetchone()
    
    # Si le trajet est trouvé, renvoyer les informations sous forme de JSON
    if trajet:
        data = dict(zip(columns, trajet))
        return JsonResponse(data, safe=False)
    else:
        return JsonResponse({'error': 'Trajet non trouvé'}, status=404)
