"""
URL configuration for monprojet project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from monservice.views import mon_service_soap
from monservice.views import tous_les_trajets
from monservice.views import trajvilles
from monservice.views import ajouter_trajet
from monservice.views import reserver_place
from monservice.views import infos_trajet
from django.urls import re_path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('soap/', mon_service_soap),
    
    #exemple d'utilisation : http://127.0.0.1:8080/toustrajets/
    path('toustrajets/', tous_les_trajets),
    
    #service qui sert a ajouter un trajet  http://127.0.0.1:8080/ajouter-trajet/Ville de départ /Ville d'arrivée /Date depart/heure depart/date arrivee/heure arrivee/nom du train/
    #exemple d'utilisation : http://127.0.0.1:8080/ajouter-trajet/Paris/Lyon/2024-01-07/15:00/2024-01-07/19:00/Train_4/
    path('ajouter-trajet/<str:depart>/<str:arrivee>/<str:date_depart>/<str:heure_depart>/<str:date_arrivee>/<str:heure_arrivee>/<str:numero_train>/', ajouter_trajet, name='ajouter_trajet'),
    
    #service qui sert a reserver une place dans un trajet  http://127.0.0.1:8000/reserver-trajet/id_trajet/classe/id_client/nombre_places/
    #exemple d'utilisation : http://127.0.0.1:8000/reserver-trajet/29/standard/1/5/
   path('reserver-trajet/<int:id_trajet>/<str:classe>/<int:id_client>/<int:nombre_places>/', reserver_place, name='reserver_place'),
    
    #service qui sert a obtenir des trajets, 
    # 1. On peut rentrer la ville de depart et la ville d'arriver, choisir les dates et les heures de depart ou d'arrivee = tout les trajet entre ces deux villes qui respectent les dates et les heures
    #    exemple d'utilisation : http://127.0.0.1:8000/trajets/?depart=Paris&arrivee=Lyon&date_depart=2024-01-06&heure_depart=15:00&heure_arivee=19:00&date_arrivee=2024-01-06
    #    On peut enlever les parametres qu'on veut
    # 2. On rentre juste la ville de départ = tout les trajet depuis cette ville  (exemple d'utilisation : http://127.0.0.1:8080/trajets/?depart=Paris)
    # 3. On rentre la ville de départ et la ville d'arrivée = tout les trajet entre ces deux villes (exemple d'utilisation : http://127.0.0.1:8080/trajets/?depart=Paris&arrivee=Lyon)
    # 4. On rentre juste la ville d'arrivée = tout les trajet vers cette ville (exemple d'utilisation :  http://127.0.0.1:8080/trajets/?arrivee=Lyon)
    # 5. On rentre rien = tout les trajet (exemple d'utilisation : http://127.0.0.1:8080/trajets)
    path('trajets/', trajvilles, name='trajets_entre_villes'),
    
    #service qui sert a obtenir les informations d'un trajet à partir de son id (exemple d'utilisation : http://127.0.0.1:8080/info-trajet/30/)
    path('info-trajet/<int:trajet_id>/', infos_trajet, name='infos_trajet'),
    

]

