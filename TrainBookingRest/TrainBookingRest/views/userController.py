from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.db import connection
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt

from rest_framework.decorators import api_view
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from .generalController import create_account

import pymysql
import pymysql.cursors
import json



def admin_user(request):


    return render(request, 'user/index_users.html')




@api_view(['GET'])
@swagger_auto_schema()
def get_data_users(request):
    db_config = {
        'host': '127.0.0.127',
        'user': 'root',
        'password': '',
        'database': 'web_service_project_mk_lm',
    }
    try:
        connection = pymysql.connect(**db_config)
        with connection.cursor() as cursor:
            query = "SELECT * FROM client"
            cursor.execute(query)
            result = cursor.fetchall()

            users_data = []
            for row in result:
                user = {
                    'name': row[1],  
                    'surname': row[2],
                    'identifiant': row[3],
                    'email': row[4],
                    'mdp': row[5],
                    'view': f'<button type="button" class="btn bi bi-eye text-primary" style="font-size: 1.3rem;" onclick="openViewModalUser({row[0]})"></button>',
                    'edit': f'<button type="button" class="btn bi bi-pencil-square text-warning" style="font-size: 1.3rem;" onclick="openEditModalUser({row[0]})"></button>',
                    'delete': f'<button type="button" class="btn bi bi-trash3-fill text-danger" style="font-size: 1.3rem;" onclick="deleteUser({row[0]})"></button>',
                }
                users_data.append(user)

    except pymysql.MySQLError as e:
        print(f"Erreur MySQL: {e}")
        return JsonResponse({'message': 'Server ERROR', 'error': 1})

    finally:
        if 'connection' in locals() and connection.open:
            connection.close()

    return JsonResponse(users_data, safe=False)



@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def users(request):
    db_config = {
        'host': '127.0.0.127',
        'user': 'root',
        'password': '',
        'database': 'web_service_project_mk_lm',
    }
    if request.method == 'POST':
        name = request.POST.get('inputName')
        surname = request.POST.get('inputSurname')
        identifiant = request.POST.get('inputUsername')
        email = request.POST.get('inputEmail')
        mdp = request.POST.get('inputMdp')
        bool = False
        data_message = []
        if name == None or name == "":
            bool = True
            data_message.append("erreur data name")
        if surname == None or surname == "":
            bool = True
            data_message.append("erreur data surname")
        if identifiant == None or identifiant == "":
            bool = True
            data_message.append("erreur data identifiant")
        if email == None or email == "":
            bool = True
            data_message.append("erreur data email")
        if mdp == None or mdp == "":
            bool = True
            data_message.append("erreur data password")
        if bool == True:
            return JsonResponse({'message': 'Erreur données envoyé à POST', 'error': 1,'describe_error':data_message})
        return create_account(request)

    if request.method == 'GET':
        user_data = []
        user_id = request.GET.get('id')
        bool = False
        data_message = []
        if user_id == None or user_id == "":
            bool = True
            data_message.append("erreur data id")
        if bool == True:
            return JsonResponse({'message': 'Erreur données envoyé à GET', 'error': 1,'describe_error':data_message})
        try:
            connection = pymysql.connect(**db_config)
            with connection.cursor() as cursor:
                query = "SELECT * FROM client WHERE id = %s"
                cursor.execute(query, (user_id,))
                result = cursor.fetchall()
                if len(result) == 0:
                    data_message.append("erreur data id. No result")
                    return JsonResponse({'message': 'Erreur données envoyé à GET', 'error': 1,'describe_error':data_message})
                for row in result:
                    print(row)
                    user = {
                        'surname': row[1],
                        'name': row[2],
                        'identifiant': row[3],
                        'email': row[4],
                        'mdp': row[5],
                    }
                    user_data.append(user)

        except pymysql.MySQLError as e:
            print(f"Erreur MySQL: {e}")
            return JsonResponse({'message': 'Server ERROR','error':1})  

        finally:
            if 'connection' in locals() and connection.open:
                connection.close()

        return JsonResponse({'message': 'GET', 'data': user_data,'error':0}) 
    if request.method == 'PUT':
        try:
            data = json.loads(request.body.decode('utf-8'))
            id_edit_user = data.get('id_edit_user')
            input_name = data.get('inputName')
            input_surname = data.get('inputSurname')
            input_username = data.get('inputUsername')
            input_email = data.get('inputEmail')
            input_mdp = data.get('inputMdp')
            bool = False
            data_message = []
            if id_edit_user == None or id_edit_user == "":
                bool = True
                data_message.append("erreur data id")
            if input_name == None or input_name == "":
                bool = True
                data_message.append("erreur data name")
            if input_surname == None or input_surname == "":
                bool = True
                data_message.append("erreur data surname")
            if input_username == None or input_username == "":
                bool = True
                data_message.append("erreur data identifiant")
            if input_email == None or input_email == "":
                bool = True
                data_message.append("erreur data email")
            if input_mdp == None or input_mdp == "":
                bool = True
                data_message.append("erreur data password")
            if bool == True:
                return JsonResponse({'message': 'Erreur données envoyé à PUT', 'error': 1,'describe_error':data_message})
            connection = pymysql.connect(**db_config)
            with connection.cursor() as cursor:
                query = "UPDATE client SET name=%s, surname=%s, identifiant=%s, email=%s, mdp=%s WHERE id=%s"
                values = (input_name, input_surname, input_username, input_email, input_mdp, id_edit_user)
                cursor.execute(query, values)
            connection.commit()
            connection.close()

            return JsonResponse({'message': 'Utilisateur mis à jour avec succès', 'error': 0})
        except json.JSONDecodeError:
            return JsonResponse({'message': 'Erreur lors de l\'analyse JSON', 'error': 1})
        except pymysql.MySQLError as e:
            return JsonResponse({'message': f'Erreur MySQL: {e}', 'error': 1})
        except Exception as e:
            return JsonResponse({'message': f'Erreur: {str(e)}', 'error': 1})

        return JsonResponse({'message': 'PUT'})     
    if request.method == 'DELETE':
        return delete_user(request)
    return JsonResponse({'message':"ERROR 404"})
 

@require_http_methods(["DELETE"])
def delete_user(request):
    db_config = {
        'host': '127.0.0.127',
        'user': 'root',
        'password': '',
        'database': 'web_service_project_mk_lm',
    }
    try:
        data = json.loads(request.body.decode('utf-8'))
        input_id = data.get('input_id')
        bool = False
        data_message = []
        if input_id == None or input_id == "":
            bool = True
            data_message.append("erreur data id")
        if bool == True:
            return JsonResponse({'message': 'Erreur données envoyé à DELETE', 'error': 1,'describe_error':data_message})
        connection = pymysql.connect(**db_config)

        with connection.cursor() as cursor:
            query = "DELETE FROM client WHERE id = %s"
            cursor.execute(query, [input_id])
        connection.commit()

        return JsonResponse({'message': 'Client supprimé avec succès', 'error': 0})

    except json.JSONDecodeError:
        return JsonResponse({'message': 'Erreur lors de l\'analyse JSON', 'error': 1})