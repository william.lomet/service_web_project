from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
import pymysql


def home(request):
    return render(request, 'home.html')

def connection_view(request):
    return render(request, 'connection.html')

def create_account(request):
    if request.method == 'POST':
        name = request.POST.get('inputName')
        surname = request.POST.get('inputSurname')
        identifiant = request.POST.get('inputUsername')
        email = request.POST.get('inputEmail')
        mdp = request.POST.get('inputMdp')
        return JsonResponse(create_client(name,surname,identifiant,email,mdp))


def create_client(name,surname,identifiant,email,mdp):
    db_config = {
        'host': '127.0.0.127',
        'user': 'root',
        'password': '',
        'database': 'web_service_project_mk_lm',
    }
    connection = None  # Initialiser la variable connection à None

    try:
        connection = pymysql.connect(**db_config)
        with connection.cursor() as cursor:
            # Vérifier si l'utilisateur existe déjà avec cet identifiant
            query_check_user = "SELECT * FROM client WHERE identifiant = %s"
            cursor.execute(query_check_user, (identifiant,))
            existing_user = cursor.fetchone()

            if existing_user:
                return {'message': 'Identifiant est déjà existant', 'error': 1}

            query_check_user = "SELECT * FROM client WHERE email = %s"
            cursor.execute(query_check_user, (email,))
            existing_email = cursor.fetchone()

            if existing_email:
                return {'message': 'Email est déjà existant', 'error': 1}       

            # Créer un nouvel utilisateur
            query_create_user = "INSERT INTO client (name,surname,identifiant,email,mdp) VALUES (%s, %s, %s, %s, %s)"
            cursor.execute(query_create_user, (name,surname,identifiant,email,mdp))
            connection.commit()
            return {'message': 'Utilisateur créé avec succès.', 'error': 0}

    except Exception as e:
        print(e)  # Ajoutez cette ligne pour imprimer le message d'erreur

        return {'message': 'Server ERROR', 'error': 1}

    finally:
        if connection:
            connection.close()
