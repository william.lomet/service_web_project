from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
import requests
import pymysql
import pymysql.cursors

from rest_framework.decorators import api_view
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi



def searchTrajet_view(request):
    city = []

    db_config = {
        'host': '127.0.0.127',
        'user': 'root',
        'password': '',
        'database': 'web_service_project_mk_lm',
    }
    try:
        connection = pymysql.connect(**db_config, cursorclass=pymysql.cursors.DictCursor)
        with connection.cursor() as cursor:
            query = "SELECT * FROM city ORDER BY name"
            cursor.execute(query)
            result = cursor.fetchall()
            for row in result:
                city.append(row['name'])
                print(row['name'])

    except pymysql.MySQLError as e:
        print(f"Erreur MySQL: {e}")
    finally:
        if 'connection' in locals() and connection.open:
            connection.close()

    context = {
        'city': city,
    }
    return render(request, 'searchTrajet.html', context)

def convert_to_new_format(item):
    return {
        "train_name": f"{item['train_name']}",
        "city_departure": item["depart_city_name"],
        "city_reach": item["arrivee_city_name"],
        "date_departure": f"{item['start_date']} {item['start_hour']}",
        "date_reach": f"{item['arrival_date']} {item['arrival_hour']}",
        "nb_place_dispo": f"{item['nb_place_disponible_all']}",
        'edit': f'<button type="button" class="btn bi bi-eye text-info" style="font-size: 1.3rem;" onclick="openModalView({item["id"]})"></button>',
        'booking': f'<button role="button" class="btn btn-primary" onclick="openModalBooking({item["id"]})">Réserver</button>'
    }




@api_view(['GET'])
@swagger_auto_schema(
    manual_parameters=[
        {
            'name': 'filter',
            'in': 'query',
            'description': 'Filter is a string that takes "false" or "true"',
            'type': 'string',
        },
    ],
    operation_id='trajet_get_data_trajet_list',  
    responses={200: openapi.Response('Description', None)},
    tags=['trajet'],
)
def get_data_trajet(request):
    if request.method == 'GET':
        filter = request.GET.get('filter')
        bool = False
        data_message = []
        if filter == None or filter == "" or (filter != "true" and filter != "false"):
            bool = True
            data_message.append("erreur filter boolean")

        if bool == True:
            return JsonResponse({'message': 'Erreur données envoyé à GET', 'error': 1,'describe_error':data_message})

        if filter == "false":
            url = 'http://127.0.0.1:8080/trajets'
            response = requests.get(url)
            data_from_server = response.json()
            data_new_format = [convert_to_new_format(item) for item in data_from_server]
        else : 
            input_date_depart = request.GET.get('input_date_depart')
            input_heure_depart = request.GET.get('input_heure_depart')
            input_date_arrive = request.GET.get('input_date_arrive')
            input_heure_arrive = request.GET.get('input_heure_arrive')
            input_ville_depart = request.GET.get('input_ville_depart')
            input_ville_arrive = request.GET.get('input_ville_arrive')
            bool = False
            data_message = []
            if input_date_depart == None:
                bool = True
                data_message.append("erreur data date depart")
            if input_heure_depart == None:
                bool = True
                data_message.append("erreur data heure depart")
            if input_date_arrive == None:
                bool = True
                data_message.append("erreur data date arrive")
            if input_heure_arrive == None:
                bool = True
                data_message.append("erreur data heure arrive")
            if input_ville_depart == None:
                bool = True
                data_message.append("erreur data ville depart")
            if input_ville_arrive == None:
                bool = True
                data_message.append("erreur data ville arrive")
            if bool == True:
                return JsonResponse({'message': 'Erreur données envoyé à POST', 'error': 1,'describe_error':data_message})
            url = f'http://127.0.0.1:8080/trajets/?depart={input_ville_depart}&arrivee={input_ville_arrive}&date_depart={input_date_depart}&heure_depart={input_heure_depart}&heure_arivee={input_heure_arrive}&date_arrivee={input_date_arrive}'
            response = requests.get(url)
            data_from_server = response.json()
            data_new_format = [convert_to_new_format(item) for item in data_from_server]
        return JsonResponse(data_new_format, safe=False)
    return JsonResponse({'error':'1','message': 'Methode non autorisé, Erreur 404'})  


@api_view(['GET'])
@swagger_auto_schema()
def trajet(request):
    if request.method == 'GET':
        trajet_id = request.GET.get('id')
        bool = False
        data_message = []
        if trajet_id == None or trajet_id == "":
            bool = True
            data_message.append("erreur data id")
        if bool == True:
            return JsonResponse({'message': 'Erreur données envoyé à GET', 'error': 1,'describe_error':data_message})
        url = f'http://127.0.0.1:8080/info-trajet/{trajet_id}'        
        response = requests.get(url)
        if response.status_code == 404:
            data_message.append("erreur serveur")
            return JsonResponse({'message': 'Erreur données envoyé à GET', 'error': 1,'describe_error':data_message})
        data_from_server = response.json()
        data = {
            'villeDepart': data_from_server["depart_city_name"],
            'villeArrive': data_from_server["arrivee_city_name"],
            'dateDepart': data_from_server["start_date"],
            'dateArrive': data_from_server["arrival_date"],
            'heureDepart': data_from_server["start_hour"],
            'heureArrive': data_from_server["arrival_hour"],
            'nomTrain': data_from_server["train_name"],
            'placeTotal': data_from_server["nb_place_disponible_all"],
            'nb_first': data_from_server["nb_place_disponible_first"],
            'nb_business': data_from_server["nb_place_disponible_premium"],
            'nb_standard': data_from_server["nb_place_disponible_standard"],
        }
        return JsonResponse({'message': 'GET','data':data,'error': 0})  
    return JsonResponse({'error':'1','message': 'Methode non autorisé, Erreur 404'})  

@api_view(['POST'])
@swagger_auto_schema()
def bookTrajet(request):
    if request.method == 'POST':

        print(request.POST)
        trajet_id = request.POST.get('idTrajet')
        identifiant = request.POST.get('inputUsername')
        mdp = request.POST.get('inputMdp')
        inputFirst = request.POST.get('inputFirst')
        inputPremium = request.POST.get('inputPremium')
        inputStandard = request.POST.get('inputStandard')
        nb_first = request.POST.get('nb_first')
        nb_business = request.POST.get('nb_business')
        nb_standard = request.POST.get('nb_standard')

        bool = False
        data_message = []
        if trajet_id == None or trajet_id == "":
            print(trajet_id)
            bool = True
            data_message.append("erreur data id")
        if identifiant == None or identifiant == "":
            bool = True
            data_message.append("erreur data identifiant")
        if mdp == None or mdp == "":
            bool = True
            data_message.append("erreur data password")

        if nb_first == None or nb_first == "" or nb_first == "0":
            bool = True
            data_message.append("erreur nb_business")
        if nb_business == None or nb_business == "" or nb_business == "0":
            bool = True
            data_message.append("erreur nb_standard")
        if nb_standard == None or nb_standard == "" or nb_standard == "0":
            bool = True
            data_message.append("erreur nb_standard")

        if inputFirst == None or int(inputFirst) < 0 or int(inputFirst) > int(nb_first):
            bool = True
            data_message.append("erreur data number place First")
        if inputPremium == None or int(inputPremium) < 0 or int(inputPremium) > int(nb_business):
            bool = True
            data_message.append("erreur data number place Premium")
        if inputStandard == None or int(inputStandard) < 0 or int(inputStandard) > int(nb_standard):
            bool = True
            data_message.append("erreur data number place Standard")


        if bool == True:
            return JsonResponse({'message': 'Erreur données envoyé à POST', 'error': 1,'describe_error':data_message})


        db_config = {
            'host': '127.0.0.127',
            'user': 'root',
            'password': '',
            'database': 'web_service_project_mk_lm',
        }
        try:
            connection = pymysql.connect(**db_config)
            with connection.cursor() as cursor:
                query = "SELECT * FROM client WHERE (identifiant = %s OR email = %s) AND mdp = %s"
                params = (identifiant, identifiant, mdp)
                cursor.execute(query, params)
                result = cursor.fetchall()
                if not result:
                    return JsonResponse({'message': 'Identifiant/Email ou Mot de passe sont incorrect'})  
                else: 
                    error = []
                    if inputFirst != "0":
                        url = f'http://localhost:8080/reserver-trajet/{trajet_id}/first/{result[0][0]}/{inputFirst}' 
                        response = requests.get(url)
                        if response.status_code != 200:
                            error.append("Serveur erreur, Place First non réservé")         
                    if inputPremium != "0":
                        url = f'http://localhost:8080/reserver-trajet/{trajet_id}/premium/{result[0][0]}/{inputPremium}'  
                        response = requests.get(url)
                        if response.status_code != 200:
                            error.append("Serveur erreur, Place Premium non réservé")         
                    if inputStandard != "0":
                        url = f'http://localhost:8080/reserver-trajet/{trajet_id}/standard/{result[0][0]}/{inputStandard}'        
                        response = requests.get(url)
                        if response.status_code != 200:
                            error.append("Serveur erreur, Place Standard non réservé")
                    if len(error) > 0:
                        return JsonResponse({'message': error,'error':1})
                    else :
                        return JsonResponse({'message': 'Reservation réalisé','error':0})  
                for row in result:
                    print(row)

        except pymysql.MySQLError as e:
            print(f"Erreur MySQL: {e}")
            return JsonResponse({'message': 'Server ERROR','error':1})  

        finally:
            if 'connection' in locals() and connection.open:
                connection.close()


        return JsonResponse({'message': 'POST','error':'0'})  
    else : 
        return JsonResponse({'error':'1','message': 'Methode non autorisé, Erreur 404'})  

