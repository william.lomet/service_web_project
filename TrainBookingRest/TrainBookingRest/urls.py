from django.contrib import admin
from django.urls import path

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from .views.generalController import home, connection_view,create_account
from .views.trajetController import searchTrajet_view, get_data_trajet, trajet,bookTrajet
from .views.userController import admin_user, get_data_users,users


schema_view = get_schema_view(
    openapi.Info(
        title="API rest- TrainBooking",
        default_version='v1',
        description="C'est notre API pour la réservation de train",
        terms_of_service="http://localhost:8000/",
        contact=openapi.Contact(email=""),
        license=openapi.License(name="Your License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)



urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('swagger(?P<format>\.json|\.yaml)', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    path('', home, name='home'),
    path('connect', connection_view, name='connection'),
    path('searchTrajet', searchTrajet_view, name='searchTrajet'),
    path('trajet/get_data_trajet', get_data_trajet, name='get_data_trajet'),
    path('trajet',trajet,name='trajet'),
    path('bookTrajet',bookTrajet,name='bookTrajet'),
    path('create_account',create_account,name='create_account'),
    path('admin_user',admin_user,name='admin_user'),
    path('users/get_data_users',get_data_users,name='get_data_users'),
    path('users',users,name='users'),



]
