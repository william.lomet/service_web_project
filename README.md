https://www-inf.telecom-sudparis.eu/SIMBAD/courses/doku.php?id=teaching_assistant:web_services:midterm2021_88




Pour lancer notre projet 

Avoir une base de donnés:
    - vous trouverez le script de la creation de la BD dans service_web_project\conception\Script_BD.sql
    - La session doit avoir 
        Nom ou IP de l'hote : 127.0.0.127
        port : 3306

Avoir python 3.11 ou 3.8
Veuillez executer les commandes suivantes pour le lancement des services:
    - Lancez un terminal et placez vous dans le dossier SERVICE_WEB_PROJECT

    - Installation des requirements :  

                pip install -r requirement.txt

    - Allez dans le dossier /TrainBookingRest

    - Lancez le service REST  : 

                python manage.py runserver 8000

    - Ouvrez un autre terminal et allez dans le dossier / ServiceSOAP

    - Lancez le service SOAP : 
        
                python manage.py runserver 8080

    - Allez sur internet et mettez ce lien : http://localhost:8080/
